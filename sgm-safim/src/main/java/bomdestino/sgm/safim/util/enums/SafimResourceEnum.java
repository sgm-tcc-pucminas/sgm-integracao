package bomdestino.sgm.safim.util.enums;

public enum SafimResourceEnum {

    SERVICO_RESPONSAVEL("/api/private/v1/servico/responsavel/"),
    SERVICO_STATUS("/api/private/v1/servico/"),
    SERVICO_EXECUCAO("/api/private/v1/servico"),
    ;

    private String value;

    SafimResourceEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
