package bomdestino.sgm.safim.service;

import bomdestino.sgm.safim.service.component.RestComponent;
import bomdestino.sgm.shared.contrato.SafimContrato;
import bomdestino.sgm.shared.dto.ExecucaoServicoDTO;
import bomdestino.sgm.shared.dto.ServicoResponsavelDTO;
import bomdestino.sgm.shared.dto.ServicoResponsavelReponseDTO;
import bomdestino.sgm.shared.dto.ServicoStatusDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static bomdestino.sgm.safim.util.enums.SafimResourceEnum.SERVICO_EXECUCAO;
import static bomdestino.sgm.safim.util.enums.SafimResourceEnum.SERVICO_RESPONSAVEL;
import static bomdestino.sgm.safim.util.enums.SafimResourceEnum.SERVICO_STATUS;
import static java.lang.String.format;

@Service
public class SafimService implements SafimContrato {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RestComponent restComponent;

    @Override
    public List<ServicoResponsavelDTO> obterServicoResponsavel(Long idServico) {
        logger.info(format("Obtendo responsavel pela execucao do servico id %d", idServico));
        ServicoResponsavelReponseDTO responsaveis = restComponent.get(SERVICO_RESPONSAVEL.getValue() + idServico,
                ServicoResponsavelReponseDTO.class);

        return responsaveis.getResponsaveis();
    }

    @Override
    public void executarServico(ExecucaoServicoDTO execucaoServico) {
        logger.info("Enviando solicitação de execução de serviço");
        restComponent.post(SERVICO_EXECUCAO.getValue(), execucaoServico, execucaoServico.getClass());
    }

    public ServicoStatusDTO obterStatusSolicitacao(Long idSolicitacaoServico) {
        return restComponent.get(SERVICO_STATUS.getValue() + idSolicitacaoServico,
                ServicoStatusDTO.class);
    }
}