package bomdestino.sgm.safim.service.component;

import bomdestino.sgm.shared.config.AuthenticationComponent;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;

@Component
public class RestComponent {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AuthenticationComponent authentication;

    @Value("${safim.url}")
    private String url;

    @Autowired
    private Gson gson;

    public <T> T get(String resource, Class<T> responseType) {
        HttpHeaders headers = createHeaders();
        HttpEntity request = new HttpEntity(headers);
        return restTemplate.exchange(getUrl(resource), HttpMethod.GET, request, responseType).getBody();
    }

    public void post(String url, Object object, Class<? extends Serializable> classType) throws RestClientException {
        HttpHeaders headers = createHeaders();
        String json = gson.toJson(object, classType);
        HttpEntity<String> request = new HttpEntity<>(json, headers);
        restTemplate.postForLocation(getUrl(url), request);
    }

    private HttpHeaders createHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(authentication.obterToken());
        return headers;
    }

    private String getUrl(String resource) {
        return url + resource;
    }
}
