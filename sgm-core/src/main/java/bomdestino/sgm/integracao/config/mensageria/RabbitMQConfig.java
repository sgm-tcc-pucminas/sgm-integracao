package bomdestino.sgm.integracao.config.mensageria;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableRabbit
public class RabbitMQConfig {

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public Queue educacaoSolicitacaoVagaQueue() {
        return new Queue("educacao-solicitacao-vaga", true, false, false);
    }

    @Bean
    public FanoutExchange educacaoSolicitacaoVagaExchange() {
        return new FanoutExchange("educacao-solicitacao-vaga");
    }

    @Bean
    Binding educacaoSolicitacaoVagaBinding(Queue educacaoSolicitacaoVagaQueue,
                                           FanoutExchange educacaoSolicitacaoVagaExchange) {

        return BindingBuilder.bind(educacaoSolicitacaoVagaQueue).to(educacaoSolicitacaoVagaExchange);
    }

    @Bean
    public Queue servicoSolicitacaoExecucaoQueue() {
        return new Queue("servico-solicitacao-execucao", true, false, false);
    }

    @Bean
    FanoutExchange servicoSolicitacaoExecucaoExchange() {
        return new FanoutExchange("servico-solicitacao-execucao");
    }

    @Bean
    Binding servicoSolicitacaoExecucaoBinding(Queue servicoSolicitacaoExecucaoQueue,
                                              FanoutExchange servicoSolicitacaoExecucaoExchange) {

        return BindingBuilder.bind(servicoSolicitacaoExecucaoQueue).to(servicoSolicitacaoExecucaoExchange);
    }

    @Bean
    public Queue integracaoExecutarServicoQueue() {
        return new Queue("integracao-executar-servico", true, false, false);
    }

    @Bean
    FanoutExchange integracaoExecutarServicoExchange() {
        return new FanoutExchange("integracao-executar-servico");
    }

    @Bean
    Binding integracaoExecutarServicoBinding(Queue integracaoExecutarServicoQueue,
                                             FanoutExchange integracaoExecutarServicoExchange) {

        return BindingBuilder.bind(integracaoExecutarServicoQueue).to(integracaoExecutarServicoExchange);
    }

    @Bean
    public Queue servicoSolicitacaoStatusQueue() {
        return new Queue("servico-solicitacao-status", true, false, false);
    }

    @Bean
    FanoutExchange servicoSolicitacaoStatusExchange() {
        return new FanoutExchange("servico-solicitacao-status");
    }

    @Bean
    Binding servicoSolicitacaoStatusBinding(Queue servicoSolicitacaoStatusQueue,
                                            FanoutExchange servicoSolicitacaoStatusExchange) {

        return BindingBuilder.bind(servicoSolicitacaoStatusQueue).to(servicoSolicitacaoStatusExchange);
    }
}
