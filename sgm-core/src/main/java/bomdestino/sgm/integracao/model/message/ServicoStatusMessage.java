package bomdestino.sgm.integracao.model.message;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDateTime;

public class ServicoStatusMessage {

    private Long idSolicitacaoServico;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataHoraExecucao;

    public Long getIdSolicitacaoServico() {
        return idSolicitacaoServico;
    }

    public ServicoStatusMessage setIdSolicitacaoServico(Long idSolicitacaoServico) {
        this.idSolicitacaoServico = idSolicitacaoServico;
        return this;
    }

    public LocalDateTime getDataHoraExecucao() {
        return dataHoraExecucao;
    }

    public ServicoStatusMessage setDataHoraExecucao(LocalDateTime dataHoraExecucao) {
        this.dataHoraExecucao = dataHoraExecucao;
        return this;
    }
}
