package bomdestino.sgm.integracao.model.message;

import java.time.LocalDateTime;

public class ServicoExecutadoMessage {

    private Long idSolicitacao;
    private LocalDateTime dataHoraExecucao;

    public Long getIdSolicitacao() {
        return idSolicitacao;
    }

    public ServicoExecutadoMessage setIdSolicitacao(Long idSolicitacao) {
        this.idSolicitacao = idSolicitacao;
        return this;
    }

    public LocalDateTime getDataHoraExecucao() {
        return dataHoraExecucao;
    }

    public ServicoExecutadoMessage setDataHoraExecucao(LocalDateTime dataHoraExecucao) {
        this.dataHoraExecucao = dataHoraExecucao;
        return this;
    }
}
