package bomdestino.sgm.integracao.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ExecucaoServicoDTO implements Serializable {

    private ServicoDTO servico;
    private EnderecoDTO endereco;

    private Long idUsuarioSolicitacao;
    private String responsavel;
    private String contato;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataHoraSolicitacao;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataHoraAprovacao;

    public ServicoDTO getServico() {
        return servico;
    }

    public ExecucaoServicoDTO setServico(ServicoDTO servico) {
        this.servico = servico;
        return this;
    }

    public EnderecoDTO getEndereco() {
        return endereco;
    }

    public ExecucaoServicoDTO setEndereco(EnderecoDTO endereco) {
        this.endereco = endereco;
        return this;
    }

    public Long getIdUsuarioSolicitacao() {
        return idUsuarioSolicitacao;
    }

    public ExecucaoServicoDTO setIdUsuarioSolicitacao(Long idUsuarioSolicitacao) {
        this.idUsuarioSolicitacao = idUsuarioSolicitacao;
        return this;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public ExecucaoServicoDTO setResponsavel(String responsavel) {
        this.responsavel = responsavel;
        return this;
    }

    public String getContato() {
        return contato;
    }

    public ExecucaoServicoDTO setContato(String contato) {
        this.contato = contato;
        return this;
    }

    public LocalDateTime getDataHoraSolicitacao() {
        return dataHoraSolicitacao;
    }

    public ExecucaoServicoDTO setDataHoraSolicitacao(LocalDateTime dataHoraSolicitacao) {
        this.dataHoraSolicitacao = dataHoraSolicitacao;
        return this;
    }

    public LocalDateTime getDataHoraAprovacao() {
        return dataHoraAprovacao;
    }

    public ExecucaoServicoDTO setDataHoraAprovacao(LocalDateTime dataHoraAprovacao) {
        this.dataHoraAprovacao = dataHoraAprovacao;
        return this;
    }
}
