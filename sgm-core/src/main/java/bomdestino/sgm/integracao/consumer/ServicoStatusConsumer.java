package bomdestino.sgm.integracao.consumer;

import bomdestino.sgm.integracao.model.message.ServicoStatusMessage;
import bomdestino.sgm.integracao.service.MineracaoService;
import bomdestino.sgm.integracao.service.ServicoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServicoStatusConsumer {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ServicoService servicoService;

    @Autowired
    private MineracaoService mineracaoService;

    @RabbitListener(queues = "servico-solicitacao-status")
    public void executar(ServicoStatusMessage message) {
        logger.info("Obtendo mensagem para obter status de solicitacao");
        servicoService.obterStatusSolicitacao(message.getIdSolicitacaoServico());
    }
}
