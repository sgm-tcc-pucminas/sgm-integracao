package bomdestino.sgm.integracao.consumer;

import bomdestino.sgm.integracao.model.message.ExecucaoServicoMessage;
import bomdestino.sgm.integracao.service.MineracaoService;
import bomdestino.sgm.integracao.service.ServicoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExecucaoServicoConsumer {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ServicoService servicoService;

    @Autowired
    private MineracaoService mineracaoService;

    @RabbitListener(queues = "servico-solicitacao-execucao")
    public void registrar(ExecucaoServicoMessage message) {
        logger.info("Obtendo mensagem para execucao de servico");
        mineracaoService.registrarExecucaoServico(message);
        servicoService.executarServico(message);
    }

    @RabbitListener(queues = "integracao-executar-servico")
    public void executar(ExecucaoServicoMessage message) {
        logger.info("Obtendo mensagem para execucao de servico com mapa");
        servicoService.executarServicoComMapa(message);
    }
}
