package bomdestino.sgm.integracao.consumer;

import bomdestino.sgm.integracao.model.message.AprovacaoVagaMessage;
import bomdestino.sgm.integracao.service.EducacaoService;
import bomdestino.sgm.integracao.service.MineracaoService;
import bomdestino.sgm.shared.dto.AprovacaoVagaDTO;
import bomdestino.sgm.shared.dto.SolicitacaoVagaDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AprovacaoVagaConsumer {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EducacaoService service;

    @Autowired
    private MineracaoService mineracaoService;

    @RabbitListener(queues = "educacao-solicitacao-vaga")
    public void listenGroupFoo(AprovacaoVagaMessage message) {
        logger.info("Obtendo mensagem para aprovacao de vaga");
        mineracaoService.registrarAprovacaoVaga(message);

        service.aprovarSolicitacaoVaga(new AprovacaoVagaDTO()
                .setIdUsuarioAprovacao(message.getIdUsuarioAprovacao())
                .setDataHoraAprovacao(message.getDataHoraAprovacao())
                .setSolicitacao(new SolicitacaoVagaDTO()
                        .setIdEscola(message.getIdEscola())
                        .setNome(message.getNome())
                        .setTelefone(message.getTelefone())
                        .setEmail(message.getEmail())));
    }
}
