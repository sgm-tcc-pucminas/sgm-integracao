package bomdestino.sgm.integracao.producer;

import bomdestino.sgm.integracao.model.message.ServicoStatusMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServicoStatusProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(ServicoStatusMessage message) {
        rabbitTemplate.convertAndSend("servico-atualizacao-status", message);
    }
}
