package bomdestino.sgm.integracao.producer;

import bomdestino.sgm.integracao.model.message.ExecucaoServicoMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MapaServicoProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(ExecucaoServicoMessage message) {
        rabbitTemplate.convertAndSend("georreferencia-obter-mapa", message);
    }
}
