package bomdestino.sgm.integracao.producer;

import bomdestino.sgm.integracao.model.message.AprovacaoVagaMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EducacaoMineradorProducer {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(AprovacaoVagaMessage message) {
        rabbitTemplate.convertAndSend("minerador-dados-educacao", message);
    }
}
