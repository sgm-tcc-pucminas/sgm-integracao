package bomdestino.sgm.integracao.service;

import bomdestino.sgm.integracao.model.message.AprovacaoVagaMessage;
import bomdestino.sgm.integracao.model.message.ExecucaoServicoMessage;
import bomdestino.sgm.integracao.producer.EducacaoMineradorProducer;
import bomdestino.sgm.integracao.producer.ServicoMineradorProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MineracaoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EducacaoMineradorProducer educacaoMineradorProducer;

    @Autowired
    private ServicoMineradorProducer servicoMineradorProducer;

    public void registrarAprovacaoVaga(AprovacaoVagaMessage message) {
        logger.info("Enviando mensagem para mineracao de dados");
        educacaoMineradorProducer.sendMessage(message);
    }

    public void registrarExecucaoServico(ExecucaoServicoMessage message) {
        logger.info("Enviando mensagem para mineracao de dados");
        servicoMineradorProducer.sendMessage(message);
    }
}