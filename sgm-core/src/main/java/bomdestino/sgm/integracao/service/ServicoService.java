package bomdestino.sgm.integracao.service;

import bomdestino.sgm.integracao.model.message.ExecucaoServicoMessage;
import bomdestino.sgm.integracao.model.message.ServicoStatusMessage;
import bomdestino.sgm.integracao.producer.MapaServicoProducer;
import bomdestino.sgm.integracao.producer.ServicoStatusProducer;
import bomdestino.sgm.safim.service.SafimService;
import bomdestino.sgm.shared.dto.EnderecoDTO;
import bomdestino.sgm.shared.dto.ExecucaoServicoDTO;
import bomdestino.sgm.shared.dto.ServicoDTO;
import bomdestino.sgm.shared.dto.ServicoResponsavelDTO;
import bomdestino.sgm.shared.dto.ServicoStatusDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.String.format;

@Service
public class ServicoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SafimService safimService;

    @Autowired
    private MapaServicoProducer mapaServicoProducer;

    @Autowired
    private ServicoStatusProducer servicoStatusProducer;

    public List<ServicoResponsavelDTO> obterResponsavel(Long idServico) {
        logger.info(format("Obtendo responsaveis pela execucao do servico id %d", idServico));
        return safimService.obterServicoResponsavel(idServico);
    }

    public void executarServico(ExecucaoServicoMessage message) {
        logger.info(format("Inicio do fluxo de execucao do servico id %d", message.getIdServico()));
        mapaServicoProducer.sendMessage(message);
    }

    public void executarServicoComMapa(ExecucaoServicoMessage message) {
        logger.info("Registrando execucao de servico com mapa");
        safimService.executarServico(new ExecucaoServicoDTO()
                .setServico(new ServicoDTO()
                        .setIdServico(message.getIdServico())
                        .setNomeServico(message.getNomeServico())
                        .setValor(message.getValor())
                        .setPrazoAtendimento(message.getPrazoAtendimento())
                        .setUnidadeMedidaPrazo(message.getUnidadeMedidaPrazo()))
                .setEndereco(new EnderecoDTO()
                        .setEndereco(message.getEndereco())
                        .setCep(message.getCep())
                        .setObservacao(message.getObservacao())
                        .setUrlMapa(message.getUrlMapa()))
                .setResponsavel(message.getResponsavel())
                .setContato(message.getContato())
                .setIdUsuarioSolicitacao(message.getIdUsuarioSolicitacao()));
    }

    public void obterStatusSolicitacao(Long idSolicitacaoServico) {
        ServicoStatusDTO statusSolicitacao = safimService.obterStatusSolicitacao(idSolicitacaoServico);
        servicoStatusProducer.sendMessage(new ServicoStatusMessage()
                .setIdSolicitacaoServico(statusSolicitacao.getIdSolicitacaoServico())
                .setDataHoraExecucao(statusSolicitacao.getDataHoraExecucao()));
    }
}