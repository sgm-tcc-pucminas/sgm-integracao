package bomdestino.sgm.integracao.service;

import bomdestino.sgm.shared.dto.AprovacaoVagaDTO;
import bomdestino.sgm.saem.service.AlunoService;
import bomdestino.sgm.saem.service.EscolaService;
import bomdestino.sgm.shared.dto.EscolaDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.lang.String.format;

@Service
public class EducacaoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EscolaService escolaService;

    @Autowired
    private AlunoService alunoService;

    public List<EscolaDTO> obterEscolas() {
        logger.info("Obter escolas da rede municipal");
        return escolaService.obter();
    }

    public EscolaDTO obterEscolaPorId(int idEscola) {
        logger.info(format("Obter escola da rede municipal com id %d", idEscola));
        return escolaService.obterPorId(idEscola);
    }

    public void aprovarSolicitacaoVaga(AprovacaoVagaDTO aprovacao) {
        logger.info(format("Aprovando vaga da escola id %d ", aprovacao.getSolicitacao().getIdEscola()));
        alunoService.cadastrar(aprovacao);
    }
}