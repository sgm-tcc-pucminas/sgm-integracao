package bomdestino.sgm.integracao.controller;

import bomdestino.sgm.integracao.service.ServicoService;
import bomdestino.sgm.shared.dto.ServicoResponsavelDTO;
import bomdestino.sgm.shared.dto.ServicoResponsavelReponseDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/public/")
public class ServicoController {

    @Autowired
    private ServicoService servicoService;

    @ApiOperation(
            value = "Obter Responsável Serviço",
            notes = "Obter lista com os responsáveis pelo atendimento do serviço informado",
            response = ServicoResponsavelReponseDTO.class
    )
    @GetMapping("/v1/servico/responsavel/{idServico}")
    public ResponseEntity<ServicoResponsavelReponseDTO> obterResponsavel(@PathVariable("idServico") Long idServico) {
        List<ServicoResponsavelDTO> responsaveis = servicoService.obterResponsavel(idServico);

        if (responsaveis.isEmpty()) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(new ServicoResponsavelReponseDTO(responsaveis));
        }
    }
}