package bomdestino.sgm.integracao.controller;

import bomdestino.sgm.integracao.service.EducacaoService;
import bomdestino.sgm.shared.dto.EscolaDTO;
import bomdestino.sgm.shared.dto.EscolaResponseDTO;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Objects.isNull;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/public")
public class EducacaoController {

    @Autowired
    private EducacaoService escolaService;

    @ApiOperation(
            value = "Obter Escolas",
            notes = "Obtem as escolas da municipal cadastradas no SAEM",
            response = EscolaResponseDTO.class
    )
    @GetMapping("/v1/educacao/escola")
    public ResponseEntity<EscolaResponseDTO> obter() {
        List<EscolaDTO> escolas = escolaService.obterEscolas();

        if (escolas.isEmpty()) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(new EscolaResponseDTO(escolas));
        }
    }

    @ApiOperation(
            value = "Obter Escola por ID",
            notes = "Obter a escola municipal cadastrada no SAEM com ID",
            response = EscolaResponseDTO.class
    )
    @GetMapping("/v1/educacao/escola/{idEscola}")
    public ResponseEntity<EscolaDTO> obter(@PathVariable("idEscola") int idEscola) {
        EscolaDTO escola = escolaService.obterEscolaPorId(idEscola);

        if (isNull(escola)) {
            return ResponseEntity.noContent().build();

        } else {
            return ResponseEntity.ok().body(escola);
        }
    }
}