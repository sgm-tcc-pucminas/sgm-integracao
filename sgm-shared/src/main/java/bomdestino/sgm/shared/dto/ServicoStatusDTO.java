package bomdestino.sgm.shared.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ServicoStatusDTO implements Serializable {

    private Long idSolicitacaoServico;
    private LocalDateTime dataHoraExecucao;

    public Long getIdSolicitacaoServico() {
        return idSolicitacaoServico;
    }

    public ServicoStatusDTO setIdSolicitacaoServico(Long idSolicitacaoServico) {
        this.idSolicitacaoServico = idSolicitacaoServico;
        return this;
    }

    public LocalDateTime getDataHoraExecucao() {
        return dataHoraExecucao;
    }

    public ServicoStatusDTO setDataHoraExecucao(LocalDateTime dataHoraExecucao) {
        this.dataHoraExecucao = dataHoraExecucao;
        return this;
    }
}
