package bomdestino.sgm.shared.dto;

public class SolicitacaoVagaDTO {

    private Long idEscola;
    private String nome;
    private String email;
    private String telefone;

    public Long getIdEscola() {
        return idEscola;
    }

    public SolicitacaoVagaDTO setIdEscola(Long idEscola) {
        this.idEscola = idEscola;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public SolicitacaoVagaDTO setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public SolicitacaoVagaDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getTelefone() {
        return telefone;
    }

    public SolicitacaoVagaDTO setTelefone(String telefone) {
        this.telefone = telefone;
        return this;
    }
}
