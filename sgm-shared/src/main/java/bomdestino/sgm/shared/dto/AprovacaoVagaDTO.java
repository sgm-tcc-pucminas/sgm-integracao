package bomdestino.sgm.shared.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDateTime;

public class AprovacaoVagaDTO {

    private SolicitacaoVagaDTO solicitacao;
    private Long idUsuarioAprovacao;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dataHoraAprovacao;

    public SolicitacaoVagaDTO getSolicitacao() {
        return solicitacao;
    }

    public AprovacaoVagaDTO setSolicitacao(SolicitacaoVagaDTO solicitacao) {
        this.solicitacao = solicitacao;
        return this;
    }

    public Long getIdUsuarioAprovacao() {
        return idUsuarioAprovacao;
    }

    public AprovacaoVagaDTO setIdUsuarioAprovacao(Long idUsuarioAprovacao) {
        this.idUsuarioAprovacao = idUsuarioAprovacao;
        return this;
    }

    public LocalDateTime getDataHoraAprovacao() {
        return dataHoraAprovacao;
    }

    public AprovacaoVagaDTO setDataHoraAprovacao(LocalDateTime dataHoraAprovacao) {
        this.dataHoraAprovacao = dataHoraAprovacao;
        return this;
    }

    @Override
    public String toString() {
        return "AprovacaoVagaDTO: {" +
                "solicitacao: {" +
                "idEscola: " + solicitacao.getIdEscola() +
                ",nome: " + solicitacao.getNome() +
                ",email: " + solicitacao.getEmail() +
                ",telefone: " + solicitacao.getTelefone() +
                "}" +
                ",idUsuarioAprovacao: " + idUsuarioAprovacao +
                ",dataHoraAprovacao: " + dataHoraAprovacao +
                '}';
    }
}
