package bomdestino.sgm.shared.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EscolaDTO implements Serializable {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("nome")
    private String nome;
    @JsonProperty("endereco")
    private String endereco;
    @JsonProperty("bairro")
    private String bairro;
    @JsonProperty("cep")
    private Integer cep;
    @JsonProperty("responsavel")
    private String responsavel;
    @JsonProperty("contato")
    private String contato;

    public Long getId() {
        return id;
    }

    public EscolaDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public EscolaDTO setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getEndereco() {
        return endereco;
    }

    public EscolaDTO setEndereco(String endereco) {
        this.endereco = endereco;
        return this;
    }

    public String getBairro() {
        return bairro;
    }

    public EscolaDTO setBairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public Integer getCep() {
        return cep;
    }

    public EscolaDTO setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public EscolaDTO setResponsavel(String responsavel) {
        this.responsavel = responsavel;
        return this;
    }

    public String getContato() {
        return contato;
    }

    public EscolaDTO setContato(String contato) {
        this.contato = contato;
        return this;
    }
}