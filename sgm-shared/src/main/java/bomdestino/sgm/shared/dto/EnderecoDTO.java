package bomdestino.sgm.shared.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EnderecoDTO implements Serializable {

    private Integer cep;
    private String endereco;
    private String observacao;
    private String urlMapa;

    public Integer getCep() {
        return cep;
    }

    public EnderecoDTO setCep(Integer cep) {
        this.cep = cep;
        return this;
    }

    public String getEndereco() {
        return endereco;
    }

    public EnderecoDTO setEndereco(String endereco) {
        this.endereco = endereco;
        return this;
    }

    public String getObservacao() {
        return observacao;
    }

    public EnderecoDTO setObservacao(String observacao) {
        this.observacao = observacao;
        return this;
    }

    public String getUrlMapa() {
        return urlMapa;
    }

    public EnderecoDTO setUrlMapa(String urlMapa) {
        this.urlMapa = urlMapa;
        return this;
    }
}