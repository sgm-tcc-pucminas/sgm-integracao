package bomdestino.sgm.shared.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;

@Component
public class AuthenticationComponent {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${seguranca.url}")
    private String url;

    @Value("${seguranca.username}")
    private String username;

    @Value("${seguranca.password}")
    private String password;

    @Value("${seguranca.login}")
    private String login;

    @Value("${seguranca.senha}")
    private String senha;

    public String obterToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setBasicAuth(username, password);

        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap();
        params.add("grant_type", "password");
        params.add("username", login);
        params.add("password", senha);

        HttpEntity<LinkedMultiValueMap<String, Object>> request = new HttpEntity(params, headers);

        Token token = restTemplate.exchange(url + "oauth/token", HttpMethod.POST, request, Token.class).getBody();
        return token.getAccess_token();
    }
}

class Token implements Serializable {
    private String access_token;

    public String getAccess_token() {
        return access_token;
    }

    public Token setAccess_token(String access_token) {
        this.access_token = access_token;
        return this;
    }
}
