package bomdestino.sgm.shared.contrato;

import bomdestino.sgm.shared.dto.ExecucaoServicoDTO;
import bomdestino.sgm.shared.dto.ServicoResponsavelDTO;
import bomdestino.sgm.shared.dto.ServicoStatusDTO;

import java.util.List;

public interface SafimContrato {

    List<ServicoResponsavelDTO> obterServicoResponsavel(Long idServico);

    void executarServico(ExecucaoServicoDTO execucaoServico);

    ServicoStatusDTO obterStatusSolicitacao(Long idSolicitacaoServico);
}
