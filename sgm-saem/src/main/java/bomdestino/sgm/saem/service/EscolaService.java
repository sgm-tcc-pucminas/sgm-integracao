package bomdestino.sgm.saem.service;

import bomdestino.sgm.shared.dto.EscolaDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.asList;

@Service
public class EscolaService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public List<EscolaDTO> obter() {
        logger.info("Obter escolas da rede municipal");
        return asList(
                new EscolaDTO()
                        .setId(1L)
                        .setNome("Branca de Neve")
                        .setEndereco("Rua Sete de Setembro, 789")
                        .setBairro("Centro")
                        .setCep(12345612)
                        .setResponsavel("João Maria")
                        .setContato("(19) 98765-4321"),
                new EscolaDTO()
                        .setId(2L)
                        .setNome("Sete Anões")
                        .setEndereco("Rua Bahia, 456")
                        .setBairro("Vila Nova")
                        .setCep(98765432)
                        .setResponsavel("Zangado")
                        .setContato("(19) 98765-1234"),
                new EscolaDTO()
                        .setId(3L)
                        .setNome("Jõao e Maria")
                        .setEndereco("Avenida Principal, 123")
                        .setBairro("Centro")
                        .setCep(65432189)
                        .setResponsavel("João Maria")
                        .setContato("(19) 91234-9876"));
    }

    public EscolaDTO obterPorId(int idEscola) {
        switch (idEscola) {
            case 1:
                return new EscolaDTO()
                        .setId(1L)
                        .setNome("Branca de Neve")
                        .setEndereco("Rua Sete de Setembro, 789")
                        .setBairro("Centro")
                        .setCep(12345612)
                        .setResponsavel("João Maria")
                        .setContato("(19) 98765-4321");

            case 2:
                return new EscolaDTO()
                        .setId(2L)
                        .setNome("Sete Anões")
                        .setEndereco("Rua Bahia, 456")
                        .setBairro("Vila Nova")
                        .setCep(98765432)
                        .setResponsavel("Zangado")
                        .setContato("(19) 98765-1234");
            case 3:
                return new EscolaDTO()
                        .setId(3L)
                        .setNome("Jõao e Maria")
                        .setEndereco("Avenida Principal, 123")
                        .setBairro("Centro")
                        .setCep(65432189)
                        .setResponsavel("João Maria")
                        .setContato("(19) 91234-9876");

            default:
                return null;
        }
    }
}