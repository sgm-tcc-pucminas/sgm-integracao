package bomdestino.sgm.saem.service;

import bomdestino.sgm.shared.dto.AprovacaoVagaDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AlunoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public void cadastrar(AprovacaoVagaDTO aprovacao) {
        StringBuilder mensagem = new StringBuilder();
        mensagem.append("\n\n [*** SAEM ***] ----------------------------------------- [*** INICIO ***]");
        mensagem.append("\n\n idEscola: " + aprovacao.getSolicitacao().getIdEscola());
        mensagem.append("\n\n usuário solicitação: " + aprovacao.getSolicitacao().getNome());
        mensagem.append("\n\n telefone: " + aprovacao.getSolicitacao().getTelefone());
        mensagem.append("\n\n email: " + aprovacao.getSolicitacao().getEmail());
        mensagem.append("\n\n data hora aprovacao: " + aprovacao.getDataHoraAprovacao());
        mensagem.append("\n\n[*** SAEM ***] ----------------------------------------- [*** FIM ***]");

        logger.info(mensagem.toString());
    }
}