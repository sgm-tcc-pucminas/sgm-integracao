FROM openjdk:8u111-jdk-alpine

VOLUME /tmp
EXPOSE 8001

ADD sgm-core/target/app.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=prod","-Duser.timezone='-03:00'","-jar","/app.jar"]
